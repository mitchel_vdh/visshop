<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Uw online webshop en visspecialist, snelle levering en een goede diversiteit aan goedkope vis producten.">
    <meta name="keywords" content="visspecialist, webwinkel, webshop, online, goedkoop">
    <meta name="author" content="henk">
    <meta name=”robots” content=”index, follow” />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">

    <link rel=icon href={{ asset('img/favicon.ico') }}>
    <title>Boxvis | Account aanmaken</title>
</head>
<body class="bg-gray-200">
<div class="flex items-center justify-center h-screen">
    <div class="w-full max-w-xs">
        <div class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
          <form action="/">
            <h2 class="text-center text-2xl font-bold mb-4 text-blue-700">Account aanmaken</h3>
            <div class="mb-4">
              <label class="block text-gray-700 text-md lg:text-lg font-bold mb-2" for="username">
                Gebruikersnaam
              </label>
              <input class="shadow appearance-none border-2 focus:border-blue-700 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Uw gebruikersnaam" required>
            </div>
            <div class="mb-4">
              <label class="block text-gray-700 text-md lg:text-lg font-bold mb-2" for="username">
                E-mailadres
              </label>
              <input class="shadow appearance-none border-2 focus:border-blue-700 rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="email" type="email" placeholder="Uw e-mailadres" required>
            </div>
            <div>
              <label class="block text-gray-700 text-md lg:text-lg font-bold mb-2" for="password">
                Wachtwoord
              </label>
              <input class="shadow appearance-none border-2 focus:border-blue-700 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password" type="password" placeholder="Uw wachtwoord" required>
            </div>
            <div class="mb-3">
              <label class="block text-gray-700 text-md lg:text-lg font-bold mb-2" for="password">
                Bevestig wachtwoord
              </label>
              <input class="shadow appearance-none border-2 focus:border-blue-700 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" id="password_2" type="password" placeholder="Bevestig uw wachtwoord" required>
            </div>
            <button class="bg-blue-700 hover:bg-blue-600 text-white font-bold py-2 px-3 rounded focus:outline-none focus:shadow-outline" type="submit">Registratie voltooien</button>
          </form>
          <div class="text-center mt-3 flex">
            <span class="h-3 w-3 ml-2 mr-1 text-blue-700"><i class="fa fa-chevron-left"></i></span>
            <a href="/login"><p class="text-md text-blue-700 hover:underline font-bold tracking-wide">Terug naar het inloggen</p></a>
          </div>
          <div class="text-center mt-3 flex">
            <span class="h-3 w-3 ml-2 mr-1 text-orange-600"><i class="fa fa-chevron-left"></i></span>
            <a href="/"><p class="text-md text-orange-600 hover:underline font-bold tracking-wide">Terugkeren naar homepagina</p></a>
          </div>
      </div>
      <p class="text-center text-gray-500 text-xs">
        &copy; 2019 Boxvis. Alle rechten voorbehouden.
      </p>
    </div>
</div>

</body>
</html>