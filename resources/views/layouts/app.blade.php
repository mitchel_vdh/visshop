<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta http-equiv="Cache-control" content="public">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Uw online webshop en visspecialist, snelle levering en een goede diversiteit aan goedkope vis producten.">
    <meta name="keywords" content="visspecialist, webwinkel, webshop, online, goedkoop">
    <meta name="author" content="henk">
    <meta name=”robots” content=”index, follow” />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">

    <link rel=icon href={{ asset('img/favicon.ico') }}>
    <title>Boxvis | Online vis webshop en visspecialist</title>
</head>
<body class="bg-white">
    <div id="app">
    {{-- Navigation  --}}
    @section('navigation')
        <section class="bg-gray-200 text-grey-darkest py-3 lg:px-4" role="alert">
          <div class="container mx-auto items-center flex leading-none">
            <span class="flex rounded-full bg-gray-400 uppercase px-3 py-2 text-xs font-semibold mr-3">Nieuw</span>
            <span class="mr-2 text-left mr-auto">Tot en met 15 augustus worden alle producten <i>gratis verzonden!</i></span>
            <span class="opacity-75 h-4 w-4"><i class="fa fa-chevron-left"></i></span>
          </div>
        </section>
        <nav class="font-sans bg-white text-center flex justify-between items-center mx-auto container overflow-hidden">
          <button class="bg-transparent text-md md:text-lg tracking-wide font-semibold hover:text-grey-dark text-grey-darkest py-2 px-3 uppercase">Home</button>
          <ul class="text-sm text-grey-dark list-reset flex items-center">
            <li class="p-3"><a href="/">
                <span class="text-black uppercase font-semibold text-xl md:text-2xl tracking-widest">Boxvis</span>
              </a>
            </li>
          </ul>
          <div class="flex items-center">
            <a href="/checkout"><i class="fa fa-shopping-cart text-white bg-orange-500 hover:bg-orange-600 py-2 px-3 text-2xl md:text-3xl" aria-hidden="true"></i></a>
            <a href="/login" class=""><button class="text-sm bg-orange-500 hover:bg-orange-600 text-md md:text-lg font-semibold tracking-widest text-white ml-4 py-2 px-3">Inloggen</button></a>
          </div>
        </nav>        
    @show
    {{-- Header  --}}
    @section('header')
      <header class="font-sans h-64 py-64 pb-64 w-full bg-blue-700 flex flex-col items-center justify-center">
        <div class="text-white flex flex-col items-center justify-center mb-24 text-3xl">
          <h1 class="px-32 py-3 bg-blue-800 text-3xl sm:text-4xl">Boxvis</h1>
          <h3 class="text-xl lg:text-3xl mt-3">Online vis webshop en visspecialist</h3>
        </div>
      </header>
    @show
    {{-- Content --}}
    <main>
        @yield('content')
    </main>
    {{-- Footer --}}
    <footer>
        @section('footer')
        <div class="">
            <div class="container p-8">
               <div class="lg:flex mb-4">
                  <div class="sm:w-2/4 h-auto">
                     <div class="text-2xl mt-4 mb-2 font-bold">Bedrijfsgegevens</div>
                     <ul class="list-reset leading-normal text-lg text-gray-800">
                        <li>Aldegondestraat 141</li>
                        <li>3817 AV Amersfoort</li>
                        <li class="mt-3">Email: <a class="text-blue-600" href="mailto:henk@visshop.nl">henk@visshop.nl</a></li>
                        <li class="mb-3">Telefoon: <a class="text-blue-600" href="tel:06-85418116">06-85418116</li></a>
                        <li>KvK: 12345678</li>
                        <li>BTW: NL235816954B02</li>
                     </ul>
                  </div>
                  <div class="sm:w-2/4 h-auto mt-4">
                     <div class="text-2xl mb-2 font-bold">Volg ons op social media</div>
                     <ul class="list-reset leading-normal">
                        <a href="#" target="_blank"><button class="bg-blue-700 rounded-lg py-2 px-3 mr-1"><i class="fa fa-facebook text-2xl text-white" aria-hidden="true"></i></button></a>
                        <a href="#" target="_blank"><button class="bg-blue-700 rounded-lg py-2 px-3 mr-1"><i class="fa fa-instagram text-2xl text-white" aria-hidden="true"></i></button></a>
                        <a href="#" target="_blank"><button class="bg-blue-700 rounded-lg py-2 px-3 mr-1"><i class="fa fa-twitter text-2xl text-white" aria-hidden="true"></i></button></a>
                        <a href="#" target="_blank"><button class="bg-blue-700 rounded-lg py-2 px-3"><i class="fa fa-linkedin text-2xl text-white" aria-hidden="true"></i></button></a>
                     </ul>
                  </div>
                  <div class="sm:w-1/2 mt-4 h-auto">
                     <div class="text-2xl mb-2 font-bold">Nieuwsbrief</div>
                     <p class="text-gray-800 text-lg leading-normal">Wilt u op de hoogte van de nieuwste producten en aanbiedingen? Meld u zich dan nu aan.</p>
                     <div class="mt-4">
                        <input type="text" class="w-full p-2 border border-gray-300 rounded text-gray-700 text-sm h-auto rounded-lg" placeholder="Uw e-mailadres">
                        <button class="w-full bg-blue-700 text-white h-auto text-lg font-bold p-3 mt-3 rounded">Inschrijven</button>
                     </div>
                  </div>
               </div>
            </div>
        </div>
        @show
    </footer>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
