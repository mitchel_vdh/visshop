<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta http-equiv="Cache-control" content="public">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Uw online webshop en visspecialist, snelle levering en een goede diversiteit aan goedkope vis producten.">
    <meta name="keywords" content="visspecialist, webwinkel, webshop, online, goedkoop">
    <meta name="author" content="henk">
    <meta name=”robots” content=”index, follow” />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">

    <link rel=icon href={{ asset('img/favicon.ico') }}>
    <title>Boxvis | Afrekenen</title>
</head>
<body class="bg-gray-200 font-sans antialiased">
   <div class="checkout container mx-auto">
      <div class="panel flex flex-col md:flex-row mb-8 shadow-lg mt-5">
         <div class="panel-left w-full md:w-2/3 bg-white rounded-l">
            <form action="/">
               <h1 class="text-3xl font-normal p-10 border-b border-solid border-gray-400">Winkelwagen bekijken / afrekenen</h1>
               <div class="p-10 pt-8 border-b border-solid border-gray-400">
                  <div class="flex items-center mb-4">
                     <div class="border-2 border-solid border-orange-500 py-2 px-3 rounded-full font-bold mr-2 text-orange-500">1</div>
                     <h2 class="text-lg font-bold">Uw gegevens</h2>
                  </div>
                  <div class="flex flex-wrap">
                     <div class="w-3/4 mb-4 ml-0 md:w-1/2-almost mb-4 md:mb-0">
                        <label for="first_name" class="block text-sm mb-2">Voornaam *</label>
                        <input id="first_name" type="text" class="w-full text-sm border-2 focus:border-blue-700 text-gray-700 rounded px-3 py-3 focus:outline-none" required>
                     </div>
                     <div class="w-3/4 md:w-1/2-almost mb-4 ml-0">
                        <label for="last_name" class="block text-sm mb-2">Achternaam *</label>
                        <input id="last_name" type="text" class="w-full text-sm border-2 focus:border-blue-700 text-gray-700 rounded px-3 py-3 focus:outline-none" required>
                     </div>
                    <div class="w-3/4 md:w-1/2-almost mb-4 ml-0">
                        <label for="email" class="block text-sm mb-2">E-mailadres *</label>
                        <input id="email" type="email" class="w-full text-sm border-2 focus:border-blue-700 text-gray-700 rounded px-3 py-3 focus:outline-none" required>
                     </div>
                     <div class="w-3/4 mb-4 ml-0">
                        <label for="zipcode" class="block text-sm mb-2">Postcode *</label>
                        <input id="zipcode" type="text" class="w-full text-sm border-2 focus:border-blue-700 text-gray-700 rounded px-3 py-3 focus:outline-none" required="">
                     </div>
                    <div class="w-3/4 mb-4 ml-0">
                        <label for="street_number" class="block text-sm mb-2">Straat en huisnummer *</label>
                        <input id="street" type="text" class="w-full text-sm border-2 focus:border-blue-700 text-gray-700 rounded px-3 py-3 focus:outline-none" required="">
                     </div>
                     <div class="w-3/4 mb-4 ml-0">
                        <label for="street_number_2" class="block text-sm mb-2">Huisnummertoevoeging (niet verplicht)</label>
                        <input id="street_number_2" type="number" class="w-full text-sm border-2 focus:border-blue-700 text-gray-700 rounded px-3 py-3 focus:outline-none">
                     </div>
                     <div class="w-3/4 mb-4 ml-0">
                        <label for="city" class="block text-sm mb-2">Woonplaats *</label>
                        <input id="city" type="text" class="w-full text-sm border-2 focus:border-blue-700 text-gray-700 rounded px-3 py-3 focus:outline-none mb-2" required="">
                        <span class="text-gray-600">*Verplicht veld</span>
                     </div>
                  </div>
               </div>
               <div class="p-10 pt-8 border-b border-solid border-gray-200">
                  <div class="flex items-center mb-4">
                     <div class="border-2 border-solid border-orange-500 py-2 px-3 rounded-full font-bold mr-2 text-orange-500">2</div>
                     <h2 class="text-lg font-bold">Betaalmethoden</h2>
                  </div>
                  <div class="w-full">
                    <label for="payment" class="block text-sm lg:text-md mb-4">Momenteel zijn alleen betalingen via iDeal beschikbaar. Klik hieronder via welke betaalmethode u wilt betalen.</label>
                    </div>
                    <button type="button" class="focus:outline-none border-2 focus:border-blue-700 rounded-lg mr-3 mb-3 md:mb-0"><img class="inline-block p-2 h-16" src="/img/paymenticons/ABNANL2A.svg"/><button>
                    <button type="button" class="focus:outline-none border-2 focus:border-blue-700 rounded-lg mr-3 mb-3 md:mb-0"><img class="inline-block p-2 h-16" src="/img/paymenticons/BUNQNL2A.svg"/><button>
                    <button type="button" class="focus:outline-none border-2 focus:border-blue-700 rounded-lg mr-3 mb-3 md:mb-0"><img class="inline-block p-2 h-16" src="/img/paymenticons/INGBNL2A.svg"/><button>
                    <button type="button" class="focus:outline-none border-2 focus:border-blue-700 rounded-lg mr-3 mb-3 md:mb-0"><img class="inline-block p-2 h-16" src="/img/paymenticons/KNABNL2H.svg"/><button>
                    <button type="button" class="focus:outline-none border-2 focus:border-blue-700 rounded-lg mr-3 mb-3 md:mb-0"><img class="inline-block p-2 h-16" src="/img/paymenticons/RABONL2U.svg"/><button>
                    <button type="button" class="focus:outline-none border-2 focus:border-blue-700 rounded-lg mr-3 mb-3 md:mb-0"><img class="inline-block p-2 h-16" src="/img/paymenticons/SNSBNL2A.svg"/><button>
                    <span class="text-center">Uw bevestiging van uw bestelling zal via de email worden verstuurd, hier zult u tevens ook een Track and Trace code ontvangen!</span>
               </div>
               <div class="p-10 pt-8">
                  <button type="submit" class="bg-green-600 text-white w-full rounded px-4 py-3 mb-6 text-lg hover:bg-green-700">Bestelling afrekenen</button>
                  <a href="/">
                    <button type="button" class="bg-red-600 text-white w-full rounded px-4 py-3 mb-6 text-lg hover:bg-red-700">Bestelling annuleren</button>
                  </a>
                  <div class="flex items-center justify-center mb-8 mt-5">
                     <svg class="mr-2" width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                           <g transform="translate(-386.000000, -863.000000)" fill="#548BC5" fill-rule="nonzero">
                              <g transform="translate(262.000000, 186.000000)">
                                 <g transform="translate(49.000000, 602.000000)">
                                    <g transform="translate(75.000000, 75.000000)">
                                       <path d="M10,20 C4.4771525,20 0,15.5228475 0,10 C0,4.4771525 4.4771525,0 10,0 C15.5228475,0 20,4.4771525 20,10 C20,15.5228475 15.5228475,20 10,20 Z M10,18 C14.418278,18 18,14.418278 18,10 C18,5.581722 14.418278,2 10,2 C5.581722,2 2,5.581722 2,10 C2,14.418278 5.581722,18 10,18 Z M8.58578644,6.58578644 C8.19526215,6.97631073 7.56209717,6.97631073 7.17157288,6.58578644 C6.78104858,6.19526215 6.78104858,5.56209717 7.17157288,5.17157288 C8.73367004,3.60947571 11.26633,3.60947571 12.8284271,5.17157288 C14.3905243,6.73367004 14.3905243,9.26632996 12.8279866,10.8288674 L10.7066662,12.9475468 C10.3158988,13.3378278 9.6827339,13.3374334 9.29245293,12.946666 C8.90217195,12.5558985 8.90256633,11.9227337 9.29333378,11.5324527 L11.4142136,9.41421356 C12.1952621,8.63316498 12.1952621,7.36683502 11.4142136,6.58578644 C10.633165,5.80473785 9.36683502,5.80473785 8.58578644,6.58578644 Z M10,16 C9.44771525,16 9,15.5522847 9,15 C9,14.4477153 9.44771525,14 10,14 C10.5522847,14 11,14.4477153 11,15 C11,15.5522847 10.5522847,16 10,16 Z"></path>
                                    </g>
                                 </g>
                              </g>
                           </g>
                        </g>
                     </svg>
                     <div>
                        <span class="font-bold">Heeft u hulp nodig?</span>
                        <span class="text-grey-darker">Aarzel niet om contact op te nemen met onze <a class="text-blue-600" href="mailto:henk@visshop.nl">klantenservice</a>!</span>
                     </div>
                  </div>
               </div>
            </form>
         </div>
         <!-- end panel-left -->
         <div class="panel-right w-full md:w-1/3 bg-blue-600 text-white rounded-r">
            <div class="p-10">
               <h2 class="font-bold text-3xl mb-4">Bestelling controleren:</h2>
               <span></span>
               <div class="mb-4">
                <span class="block text-white text-3xl">Haring</span>
                <span class="block text-white text-lg">2 stuks</span>
                <span class="block text-white text-lg">Totaal: €4,80</span>
               </div>
              <div class="mb-4">
                <span class="block text-white text-3xl">Lekkerbek</span>
                <span class="block text-white text-lg">1 stuk</span>
                <span class="block text-white text-lg">Totaal: €2,50</span>
               </div>
              <div class="mb-4">
                <span class="block text-white text-3xl">Schol</span>
                <span class="block text-white text-lg">2 stuks</span>
                <span class="block text-white text-lg">Totaal: €8,80</span>
               </div>
               <div class="italic w-3/4 leading-normal mb-8">
                  Alle prijzen zijn incl. BTW
               </div>
               <div class="border-t-2 border-gray-500 flex flex-col lg:flex-row justify-between">
                 <span class="align-left text-3xl">Totaalprijs:</span> 
                 <span class="align-right text-3xl">€16,10</span> 
               </div>
            </div>
         </div>
      </div>
      <div class="copyright text-grey-dark text-center text-sm mb-8">
        &copy; 2019 Boxvis. Alle rechten voorbehouden.
      </div>
   </div>
</body>
</html>