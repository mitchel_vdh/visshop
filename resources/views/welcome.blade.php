@extends('layouts.app')

@section('content')

<div class="container mx-auto mt-10 pb-12">
  <h1 class="w-full my-2 text-4xl lg:text-5xl font-bold leading-tight uppercase text-center text-gray-800 mb-5">Ons assortiment</h1>
  <div class="flex flex-wrap">
    <div class="mt-10 mb-10 sm:mt-3 sm:mb-6 lg:w-1/2">
      <img class="float-left w-1/2 mr-3 h-full rounded-lg shadow" src="{{ asset('img/producten/productharing.jpg') }}" alt="haring">
      <h3 class="text-2xl font-medium font-bold mb-1 mt-1">Haring</h3>
      <span class="text-fish text-lg mr-3">
       Haring is een echte Hollandse lekkernij met een sterke uitgesproken smaak. Meestal wordt haring genuttigd bij een haringkraam met wat uitjes eroverheen, ook op een wit bolletje wordt haring op deze manier vaak gegeten. Maar naast deze traditionele manieren van haring happen zijn er nog allerlei andere recepten met haring mogelijk. Doe bijvoorbeeld eens haring in stukjes door een salade of kies een van de andere recepten met haring als ingrediënt. Haring kan ook goed dienen als borrelhapje bij een gezellig avondje met vrienden. Kijk hieronder bij de haringrecepten om inspiratie op te doen en kies een heerlijk recept uit.
      </span>
      <button class="bg-blue-700 hover:bg-blue-600 text-sm md:text-lg text-white py-2 px-3 mt-3 rounded-lg" data-toggle="modal" data-target="#haringModal">Klik hier voor meer info</button>
    </div>
    <!-- Haring Modal -->
    <div class="modal fade" id="haringModal" tabindex="-1" role="dialog" aria-labelledby="haringModal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2 class="modal-title text-2xl font-bold ml-24" id="haring-Modal">Haring €2,40 per stuk</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="text-md">
              <img src="{{ asset('img/producten/productharing.jpg') }}" class="card-img-top rounded-lg mb-4" title="haring" alt="haring">
              <p>Haring is een echte Hollandse lekkernij met een sterke uitgesproken smaak. Meestal wordt haring genuttigd
              bij een haringkraam met wat uitjes eroverheen, ook op een wit bolletje wordt haring op deze manier vaak gegeten.
              Maar naast deze traditionele manieren van haring happen zijn er nog allerlei andere recepten met haring mogelijk.
              Doe bijvoorbeeld eens haring in stukjes door een salade of kies een van de andere recepten met haring als ingredient.
              Haring kan ook goed dienen als borrelhapje bij een gezellig avondje met vrienden. Kijk hieronder bij de haringrecepten
              om inspiratie op te doen en kies een heerlijk recept uit.</p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="bg-red-700 px-3 py-2 text-white rounded" data-dismiss="modal">Doorgaan met winkelen</i></button>
            <button type="button" class="bg-blue-700 px-3 py-2 text-white rounded" data-dismiss="modal">Toevoegen aan winkelwagen</i></button>
          </div>
        </div>
      </div>
    </div>
    <div class="mt-10 mb-10 sm:mt-3 sm:mb-6 lg:w-1/2">
      <img class="float-left w-1/2 mr-3 h-full rounded-lg shadow" src="{{ asset('img/producten/productkibbeling.jpg') }}" alt="kibbeling">
      <h3 class="text-2xl font-medium font-bold mb-1 mt-1">Kibbeling</h3>
      <span class="text-fish text-lg mr-3">
       Een oer-Hollandse snack, waarbij stukken vis door een beslag worden gehaald en in hete olie worden gelegd. Voordat de vis door het beslag gaat, kun je ook nog wat verse kruiden toevoegen om de vis extra smaak te geven.
      </span>
      <button class="bg-blue-700 hover:bg-blue-600 text-sm md:text-lg text-white py-2 px-3 mt-3 rounded-lg" data-toggle="modal" data-target="#kibbelingModal">Klik hier voor meer info</button>
    </div>
    <!-- Kibbeling Modal -->
    <div class="modal fade" id="kibbelingModal" tabindex="-1" role="dialog" aria-labelledby="kibbelingModal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2 class="modal-title text-2xl font-bold ml-24" id="kibbeling-Modal">Kibbeling €2,00 per portie</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="text-md">
              <img src="{{ asset('img/producten/productkibbeling.jpg') }}" class="card-img-top rounded-lg mb-4" title="kibbeling" alt="kibbeling">
                <p>
                Een oer-Hollandse snack, waarbij stukken vis door een beslag worden gehaald en in hete olie worden gelegd. Voordat de vis door het beslag gaat, kun je ook nog wat verse kruiden toevoegen om de vis extra smaak te geven.
                </p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="bg-red-700 px-3 py-2 text-white rounded" data-dismiss="modal">Doorgaan met winkelen</i></button>
            <button type="button" class="bg-blue-700 px-3 py-2 text-white rounded" data-dismiss="modal">Toevoegen aan winkelwagen</i></button>
          </div>
        </div>
      </div>
    </div>
    <div class="mt-10 mb-10 sm:mt-3 sm:mb-6 lg:w-1/2">
      <img class="float-left w-1/2 mr-3 h-full rounded-lg shadow" src="{{ asset('img/producten/productlekkerbek.jpg') }}" alt="lekkerbek">
      <h3 class="text-2xl font-medium font-bold mb-1 mt-1">Lekkerbek</h3>
      <span class="text-fish text-lg mr-3">
       Het lekkerbekje is een filet van heek wat door een beslag van water en meel wordt gehaald waarna ze gebakken worden. Een lekkerbekje is natuurlijk het lekkerst als u direct na het bakken gaat eten. Maar als dit niet mogelijk is, kunt u het lekkerbekje ook heel gemakkelijk weer opwarmen. ± 5 minuten in een voorverwarmde oven of eventjes kort voorverwarmen in een droge koekenpan. In de magnetron is niet aan te raden. Omdat het vocht in de vis verdampt wordt deze hierdoor droog.
      </span>
      <button class="bg-blue-700 hover:bg-blue-600 text-sm md:text-lg text-white py-2 px-3 mt-3 rounded-lg" data-toggle="modal" data-target="#lekkerbekModal">Klik hier voor meer info</button>
    </div>
   <!-- Lekkerbek Modal -->
    <div class="modal fade" id="lekkerbekModal" tabindex="-1" role="dialog" aria-labelledby="lekkerbekModal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2 class="modal-title text-2xl font-bold ml-24" id="lekkerbek-Modal">Lekkerbek €2,50 per stuk</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="text-md">
              <img src="{{ asset('img/producten/productlekkerbek.jpg') }}" class="card-img-top rounded-lg mb-4" title="lekkerbek" alt="lekkerbek">
                <p>
                Het lekkerbekje is een filet van heek wat door een beslag van water en meel wordt gehaald waarna ze gebakken worden. Een lekkerbekje is natuurlijk het lekkerst als u direct na het bakken gaat eten. Maar als dit niet mogelijk is, kunt u het lekkerbekje ook heel gemakkelijk weer opwarmen. ± 5 minuten in een voorverwarmde oven of eventjes kort voorverwarmen in een droge koekenpan. In de magnetron is niet aan te raden. Omdat het vocht in de vis verdampt wordt deze hierdoor droog.
                </p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="bg-red-700 px-3 py-2 text-white rounded" data-dismiss="modal">Doorgaan met winkelen</i></button>
            <button type="button" class="bg-blue-700 px-3 py-2 text-white rounded" data-dismiss="modal">Toevoegen aan winkelwagen</i></button>
          </div>
        </div>
      </div>
    </div>
    <div class="mt-10 mb-10 sm:mt-3 sm:mb-6 lg:w-1/2">
      <img class="float-left w-1/2 mr-3 h-full rounded-lg shadow" src="{{ asset('img/producten/productmakreel.jpg') }}" alt="makreel">
      <h3 class="text-2xl font-medium font-bold mb-1 mt-1">Makreel</h3>
      <span class="text-fish text-lg mr-3">
       Makreel staat bekend om haar gezonde vetten. Er zit dan ook 10 tot 15 % vet in. Maar, gezonde vetten die je gewoon nodig hebt. Reden genoeg om eens vaker makreel te eten. Het meest gangbaar is de gerookte makreel die je tegenwoordig ook in elke supermarkt kunt kopen.
      </span>
      <button class="bg-blue-700 hover:bg-blue-600 text-sm md:text-lg text-white py-2 px-3 mt-3 rounded-lg" data-toggle="modal" data-target="#makreelModal">Klik hier voor meer info</button>
    </div>
   <!-- Makreel Modal -->
    <div class="modal fade" id="makreelModal" tabindex="-1" role="dialog" aria-labelledby="makreelModal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2 class="modal-title text-2xl font-bold ml-24" id="makreel-Modal">Makreel €2,50 per stuk</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="text-md">
              <img src="{{ asset('img/producten/productmakreel.jpg') }}" class="card-img-top rounded-lg mb-4" title="makreel" alt="makreel">
                <p>
                Makreel staat bekend om haar gezonde vetten. Er zit dan ook 10 tot 15 % vet in. Maar, gezonde vetten die je gewoon nodig hebt. Reden genoeg om eens vaker makreel te eten. Het meest gangbaar is de gerookte makreel die je tegenwoordig ook in elke supermarkt kunt kopen.
                </p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="bg-red-700 px-3 py-2 text-white rounded" data-dismiss="modal">Doorgaan met winkelen</i></button>
            <button type="button" class="bg-blue-700 px-3 py-2 text-white rounded" data-dismiss="modal">Toevoegen aan winkelwagen</i></button>
          </div>
        </div>
      </div>
    </div>
    <div class="mt-10 mb-10 sm:mt-3 sm:mb-6 lg:w-1/2">
      <img class="float-left w-1/2 mr-3 h-full rounded-lg shadow" src="{{ asset('img/producten/productzalm.jpg') }}" alt="zalm">
      <h3 class="text-2xl font-medium font-bold mb-1 mt-1">Zalm</h3>
      <span class="text-fish text-lg mr-3">
       Zalm is een heerlijke zachte vis die je op allerlei manieren kunt eten. Bak bijvoorbeeld een stuk zalm in een grillpan en je hebt eens wat anders dan een stuk vlees bij de aardappelen en groente. Gerookte zalmfilet is heerlijk op brood met een laagje roomkaas en wat sla, daarnaast kun je dit ook toevoegen in salades en kun je er een zalmcocktail van maken. Ook kun je zelf zalmsalade maken met zalm uit een blikje, kortom: tal van mogelijkheden! Ga snel met zalm aan de slag en maak de heerlijkste maaltijden met deze lekkere vis.
      </span>
      <button class="bg-blue-700 hover:bg-blue-600 text-sm md:text-lg text-white py-2 px-3 mt-3 rounded-lg" data-toggle="modal" data-target="#zalmModal">Klik hier voor meer info</button>
    </div>
   <!-- Zalm Modal -->
    <div class="modal fade" id="zalmModal" tabindex="-1" role="dialog" aria-labelledby="zalmModal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2 class="modal-title text-2xl font-bold ml-24" id="zalm-Modal">Zalm €4,00 per stuk</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="text-md">
              <img src="{{ asset('img/producten/productzalm.jpg') }}" class="card-img-top rounded-lg mb-4" title="zalm" alt="zalm">
                <p>
                Zalm is een heerlijke zachte vis die je op allerlei manieren kunt eten. Bak bijvoorbeeld een stuk zalm in een grillpan en je hebt eens wat anders dan een stuk vlees bij de aardappelen en groente. Gerookte zalmfilet is heerlijk op brood met een laagje roomkaas en wat sla, daarnaast kun je dit ook toevoegen in salades en kun je er een zalmcocktail van maken. Ook kun je zelf zalmsalade maken met zalm uit een blikje, kortom: tal van mogelijkheden! Ga snel met zalm aan de slag en maak de heerlijkste maaltijden met deze lekkere vis.
                </p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="bg-red-700 px-3 py-2 text-white rounded" data-dismiss="modal">Doorgaan met winkelen</i></button>
            <button type="button" class="bg-blue-700 px-3 py-2 text-white rounded" data-dismiss="modal">Toevoegen aan winkelwagen</i></button>
          </div>
        </div>
      </div>
    </div>
    <div class="mt-10 mb-10 sm:mt-3 sm:mb-6 lg:w-1/2">
      <img class="float-left w-1/2 mr-3 h-full rounded-lg shadow" src="{{ asset('img/producten/productschol.jpg') }}" alt="schol">
      <h3 class="text-2xl font-medium font-bold mb-1 mt-1">Schol</h3>
      <span class="text-fish text-lg mr-3">
       Schol is een van de meest gevangen vissoorten van Nederland. Toch komt de vis niet in de top 10 van meest gegeten vissoorten voor. En dat terwijl het zo’n lekkere en makkelijke vis is waar ook goed mee gevarieerd kan worden. Echt, iedereen kan schol klaarmaken.
      </span>
      <button class="bg-blue-700 hover:bg-blue-600 text-sm md:text-lg text-white py-2 px-3 mt-3 rounded-lg" data-toggle="modal" data-target="#scholModal">Klik hier voor meer info</button>
    </div>
   <!-- Schol Modal -->
    <div class="modal fade" id="scholModal" tabindex="-1" role="dialog" aria-labelledby="scholModal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2 class="modal-title text-2xl font-bold ml-24" id="schol-Modal">Schol €4,40 per stuk</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="text-md">
              <img src="{{ asset('img/producten/productschol.jpg') }}" class="card-img-top rounded-lg mb-4" title="schol" alt="schol">
                <p>
                Schol is een van de meest gevangen vissoorten van Nederland. Toch komt de vis niet in de top 10 van meest gegeten vissoorten voor. En dat terwijl het zo’n lekkere en makkelijke vis is waar ook goed mee gevarieerd kan worden. Echt, iedereen kan schol klaarmaken.
                </p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="bg-red-700 px-3 py-2 text-white rounded" data-dismiss="modal">Doorgaan met winkelen</i></button>
            <button type="button" class="bg-blue-700 px-3 py-2 text-white rounded" data-dismiss="modal">Toevoegen aan winkelwagen</i></button>
          </div>
        </div>
      </div>
    </div>
    <div class="mt-10 mb-10 sm:mt-3 sm:mb-6 lg:w-1/2">
      <img class="float-left w-1/2 mr-3 h-full rounded-lg shadow" src="{{ asset('img/producten/productgarnaal.jpg') }}" alt="garnaal">
      <h3 class="text-2xl font-medium font-bold mb-1 mt-1">Garnalen</h3>
      <span class="text-fish text-lg mr-3">
       De Hollandse garnaal is klein en daarom machinaal moeilijk te pellen. Eenmaal gevangen worden de garnalen gespoeld en gelijk gekookt. Het koken wordt bewust gedaan omdat ze dan direct gedesinfecteerd worden. Koken heeft ook een conserverende werking. De garnalen worden hierdoor langer houdbaar. Door het koken trekken de garnalen krom. Dat is later weer makkelijker met pellen. Door de fijne structuur kan de garnaal het beste ‘naturel’ gegeten worden. Bijvoorbeeld in koude gerechten zoals cocktails, mousses en salades. In warme gerechten kun je garnalen prima gebruiken als garnituur bij soepen en sauzen. Je hoeft geen topkok te zijn om mooie gerechtjes te maken van garnalen. De schalen van de garnaal zijn overigens heel geschikt om bouillon van te maken. Maak garnalen altijd vlak voor het opdienen klaar en warm ze achteraf niet weer op. Dit komt de smaak absoluut niet ten goede. Noordzeegarnalen eet u zo vers mogelijk.
      </span>
      <button class="bg-blue-700 hover:bg-blue-600 text-sm md:text-lg text-white py-2 px-3 mt-3 rounded-lg" data-toggle="modal" data-target="#garnaalModal">Klik hier voor meer info</button>
    </div>
   <!-- Garnalen Modal -->
    <div class="modal fade" id="garnaalModal" tabindex="-1" role="dialog" aria-labelledby="garnaalModal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2 class="modal-title text-2xl font-bold ml-24" id="garnaal-Modal">Garnalen €3,00 per portie</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="text-md">
              <img src="{{ asset('img/producten/productgarnaal.jpg') }}" class="card-img-top rounded-lg mb-4" title="garnaal" alt="garnaal">
                <p>
                De Hollandse garnaal is klein en daarom machinaal moeilijk te pellen. Eenmaal gevangen worden de garnalen gespoeld en gelijk gekookt. Het koken wordt bewust gedaan omdat ze dan direct gedesinfecteerd worden. Koken heeft ook een conserverende werking. De garnalen worden hierdoor langer houdbaar. Door het koken trekken de garnalen krom. Dat is later weer makkelijker met pellen. Door de fijne structuur kan de garnaal het beste ‘naturel’ gegeten worden. Bijvoorbeeld in koude gerechten zoals cocktails, mousses en salades. In warme gerechten kun je garnalen prima gebruiken als garnituur bij soepen en sauzen. Je hoeft geen topkok te zijn om mooie gerechtjes te maken van garnalen. De schalen van de garnaal zijn overigens heel geschikt om bouillon van te maken. Maak garnalen altijd vlak voor het opdienen klaar en warm ze achteraf niet weer op. Dit komt de smaak absoluut niet ten goede. Noordzeegarnalen eet u zo vers mogelijk.
                </p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="bg-red-700 px-3 py-2 text-white rounded" data-dismiss="modal">Doorgaan met winkelen</i></button>
            <button type="button" class="bg-blue-700 px-3 py-2 text-white rounded" data-dismiss="modal">Toevoegen aan winkelwagen</i></button>
          </div>
        </div>
      </div>
    </div>
    <div class="mt-10 mb-10 sm:mt-3 sm:mb-6 lg:w-1/2">
      <img class="float-left w-1/2 mr-3 h-full rounded-lg shadow" src="{{ asset('img/producten/productpaling.jpg') }}" alt="paling">
      <h3 class="text-2xl font-medium font-bold mb-1 mt-1">Paling</h3>
      <span class="text-fish text-lg mr-3">
       Vers gerookte gefileerde paling zonder huid en graat. Ook lekker voor op toast, in salade of op broodje. Heerlijk zacht, onweerstaanbaar! Ook ideaal om in te vriezen. Wil u liever zelf Paling Fileren? Bekijken dan ook eens onze hele gerookte paling.
      </span>
      <button class="bg-blue-700 hover:bg-blue-600 text-sm md:text-lg text-white py-2 px-3 mt-3 rounded-lg" data-toggle="modal" data-target="#palingModal">Klik hier voor meer info</button>
    </div>
  </div>
   <!-- Paling Modal -->
    <div class="modal fade" id="palingModal" tabindex="-1" role="dialog" aria-labelledby="palingModal" aria-hidden="true">
      <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h2 class="modal-title text-2xl font-bold ml-24" id="paling-Modal">Paling €6,00 per stuk</h2>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="text-md">
              <img src="{{ asset('img/producten/productpaling.jpg') }}" class="card-img-top rounded-lg mb-4" title="paling" alt="paling">
                <p>
                Vers gerookte gefileerde paling zonder huid en graat. Ook lekker voor op toast, in salade of op broodje. Heerlijk zacht, onweerstaanbaar! Ook ideaal om in te vriezen. Wil u liever zelf Paling Fileren? Bekijken dan ook eens onze hele gerookte paling.
                </p>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="bg-red-700 px-3 py-2 text-white rounded" data-dismiss="modal">Doorgaan met winkelen</i></button>
            <button type="button" class="bg-blue-700 px-3 py-2 text-white rounded" data-dismiss="modal">Toevoegen aan winkelwagen</i></button>
          </div>
        </div>
      </div>
    </div>
</div>



<div class="bg-gray-100 mt-24 font-sans">
  <div class="container mx-auto px-2 pt-4 pb-12">
    <h1 class="w-full my-2 text-6xl font-semibold tracking-wide leading-tight text-center text-gray-800 uppercase mb-2 mt-5">De visbox</h1>
    <p class="text-lg font-normal font-gray-600 text-center mb-16">Weet u niet wat u wilt? Laat onze specialisten een box voor u samenstellen.</p>
    <div class="flex flex-col sm:flex-row justify-center pt-12 my-12 sm:my-4">
      <div class="flex flex-col w-5/6 w-1/4 mx-auto lg:mx-0 rounded-none lg:rounded-l-lg bg-white mt-4 mb-4">

        <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
          <div class="p-8 text-3xl text-blue-700 uppercase font-bold text-center border-b-4 border-blue-500">Small box</div>
          <ul class="w-full text-center text-base font-bold">
            <li class="border-b py-4">100% Vers</li>
            <li class="border-b py-4">Snelle levering</li>
            <li class="border-b py-4">2 soorten vis</li>
          </ul>
        </div>
        <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
          <div class="w-full pt-6 text-3xl font-bold text-center">€9,99 <span class="text-base">/ per box</span></div>
          <div class="flex items-center justify-center">
            <button class="mx-auto lg:mx-0 hover:bg-orange-600 bg-orange-500 text-lg uppercase text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg">Bestel nu</button>
          </div>
        </div>
      </div>
      <div class="flex flex-col w-5/6 w-1/3 mx-auto lg:mx-0 rounded-lg bg-white sm:mt-0 sm:-mt-6 shadow-lg z-10">
        <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
          <div class="w-full p-8 text-4xl uppercase text-blue-700 font-bold text-center border-b-4 border-blue-500">Medium box</div>
          <div class="h-1 w-full gradient my-0 py-0 rounded-t"></div>
          <ul class="w-full text-center text-lg font-bold">
            <li class="border-b py-4">100% Vers</li>
            <li class="border-b py-4">Snelle levering</li>
            <li class="border-b py-4">5 soorten vis</li>
          </ul>
        </div>
        <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
          <div class="w-full pt-6 text-4xl font-bold text-center">€14,99 <span class="text-base">/ per box</span></div>
          <div class="flex items-center justify-center">
            <button class="mx-auto lg:mx-0 hover:bg-orange-600 bg-orange-500 text-lg uppercase text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg">Bestel nu</button>
          </div>
        </div>
      </div>
      <div class="flex flex-col w-5/6 w-1/4 mx-auto lg:mx-0 rounded-none lg:rounded-l-lg bg-white mt-4 mb-4">
        <div class="flex-1 bg-white rounded-t rounded-b-none overflow-hidden shadow">
          <div class="p-8 text-3xl text-blue-700 uppercase font-bold text-center border-b-4 border-blue-500">Large box</div>
          <ul class="w-full text-center text-base font-bold">
            <li class="border-b py-4">100% Vers</li>
            <li class="border-b py-4">Snelle levering</li>
            <li class="border-b py-4">7 soorten vis</li>
          </ul>
        </div>
        <div class="flex-none mt-auto bg-white rounded-b rounded-t-none overflow-hidden shadow p-6">
          <div class="w-full pt-6 text-3xl font-bold text-center">€19,99 <span class="text-base">/ per box</span></div>
          <div class="flex items-center justify-center">
            <button class="mx-auto lg:mx-0 hover:bg-orange-600 bg-orange-500 text-lg uppercase text-white font-bold rounded-full my-6 py-4 px-8 shadow-lg">Bestel nu</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container mx-auto text-center lg:flex lg:flex-row lg:justify-around mt-32 pb-48 font-sans">
    <div class="mt-4">
      <h3 class="text-6xl mb-2 font-bold text-orange-500">48</h3>
      <p class="text-2xl">Tevreden klanten</p>
    </div>
    <div class="mt-4">
      <h3 class="text-6xl mb-2 font-bold text-blue-700">120</h3>
      <p class="text-2xl">Boxen verkocht</p>
    </div>
    <div class="mt-4">
      <h3 class="text-6xl mb-2 font-bold text-orange-500">100%</h3>
      <p class="text-2xl">Viskwaliteit</p>
    </div>
  </div>
  </div>
</div>

@endsection
